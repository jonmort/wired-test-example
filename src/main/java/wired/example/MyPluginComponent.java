package wired.example;

public interface MyPluginComponent
{
    String getName();
}
