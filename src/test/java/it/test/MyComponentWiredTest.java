package it.test;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import wired.example.MyPluginComponent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AtlassianPluginsTestRunner.class)
public class MyComponentWiredTest {
    private final ApplicationProperties applicationProperties;
    private final MyPluginComponent myPluginComponent;
    private final JiraAuthenticationContext authenticationContext;
    private final UserManager userManager;
    private ApplicationUser existingUser;

    public MyComponentWiredTest(ApplicationProperties applicationProperties,
                                MyPluginComponent myPluginComponent,
                                JiraAuthenticationContext authenticationContext,
                                UserManager userManager) {
        this.applicationProperties = applicationProperties;
        this.myPluginComponent = myPluginComponent;
        this.authenticationContext = authenticationContext;
        this.userManager = userManager;
    }

    @Before
    public void setuser() {
        //existingUser = authenticationContext.getUser();
        //authenticationContext.setLoggedInUser(userManager.getUserByName("admin"));
    }

    @After
    public void removeUser() {
        //authenticationContext.setLoggedInUser(existingUser);
    }

    @Test
    public void testMyName() {
        assertNotNull(authenticationContext.getUser());
        assertEquals("admin", authenticationContext.getUser().getName());
        assertEquals("names do not match!", "myComponent:" + applicationProperties.getDisplayName(), myPluginComponent.getName());
    }
}
